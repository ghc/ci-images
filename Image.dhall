--| A description of a Docker image

let
  CF = ./deps/Containerfile.dhall

let
  --| The name a of a Docker image
  ImageName = Text

-- Note [Specifying custom platform]
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- For reasons that aren't entirely clear, Kaniko at some point started refusing
-- to use base images that don't match the builder's platform. This affects ARMv7
-- and i386 images as they are built on AArch64 and x86-64 machiners, respectively.
-- To work around this we pass the `--custom-platform` flag to kaniko, forcing it
-- to accept the upstream base images, despite the platform mis-match.
--
-- See https://github.com/GoogleContainerTools/kaniko/issues/1995

let
  type: Type =
  { name: ImageName
  , runnerTags: List Text
    -- See Note [Specifying custom platform]
  , customPlatform: Optional Text
  , jobStage: Text
  , needs: List ImageName
  , image: CF.Type
  }
in
{ Type = type
, default =
  { customPlatform = None Text
  , needs = [] : List ImageName
  , jobStage = "build"
  }
, ImageName = ImageName
}
