let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall

let
  BindistSpec: Type =
    { version : Text
    , triple : Text
    }

let Bindist = < BindistSpec : BindistSpec | BindistURL : Text >

let
  Options =
    { Type =
      { bindist : Bindist
      , destDir : Text
      , configureOpts : List Text
      }
    , default =
      { configureOpts = [] : List Text
      }
    }

let
  install: Options.Type -> CF.Type =
    \(opts: Options.Type) ->
        let
          bindistUrl: Text = merge { BindistSpec = \(bindistSpec : BindistSpec) -> "https://downloads.haskell.org/~ghc/${bindistSpec.version}/ghc-${bindistSpec.version}-${bindistSpec.triple}.tar.xz", BindistURL = \(bindistURL : Text) -> bindistURL } opts.bindist
        in
          CF.run "install GHC bindist"
          [ "curl -L ${bindistUrl} | tar -Jx -C /tmp"
          , "cd /tmp/ghc-*"
          , "./configure ${Prelude.Text.concatSep " " opts.configureOpts} --prefix=${opts.destDir}"
          , "make install"
          , "rm -Rf /tmp/ghc-*"
          , "${opts.destDir}/bin/ghc --version"
          ]

in
{ Options = Options
, BindistSpec = BindistSpec
, Bindist = Bindist
, install = install
}
