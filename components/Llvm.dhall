let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall

let
  BindistSpec: Type =
      { version : Text
      , triple : Text
      }
let
  Output =
    { Type = { action : CF.Type
             , out_llc : Text
             , out_opt : Text
             , out_clang : Text
             }
    , default = {}
    }

let Config = Output.Type

let
  installFromBindistTo: Text -> BindistSpec -> Output.Type =
    \(destDir: Text) -> \(bindist: BindistSpec) ->
      let
        url: Text = "https://github.com/llvm/llvm-project/releases/download/llvmorg-${bindist.version}/clang+llvm-${bindist.version}-${bindist.triple}.tar.xz"

      in
        { action = CF.run "install LLVM"
                     [ "curl -L ${url} | tar -xJC ."
                     , "mkdir ${destDir}"
                     , "cp -R clang+llvm*/* ${destDir}"
                     , "rm -R clang+llvm*"
                     , "${destDir}/bin/llc --version"
                     ]
        , out_llc = "${destDir}/bin/llc"
        , out_opt = "${destDir}/bin/opt"
        , out_clang="${destDir}/bin/clang" }

let
  llvmInstall: Optional Output.Type -> CF.Type =
   \(output : Optional Output.Type) ->
    merge { Some = \(llvm_out : Output.Type) -> llvm_out.action
          , None = [] : CF.Type
          } output

let
  llvmSetEnv: Output.Type -> CF.Type =
    \(output : Output.Type) ->
      CF.env (toMap
        { LLC = output.out_llc
        , OPT = output.out_opt
        , LLVMAS = output.out_clang
        })

let setEnv =
   \(output : Optional Output.Type) ->
    merge { Some = \(llvm_out : Output.Type) -> llvmSetEnv llvm_out
          , None = [] : CF.Type
          } output

let
--  llvmConfigureArgs : Optional Output.Type -> List Text
  llvmConfigureArgs = \(output: Optional Output.Type) ->
    merge { Some = \(out: Output.Type) -> [ "LLC=${out.out_llc}"
                                               , "OPT=${out.out_opt}"
                                               , "LLVMAS=${out.out_clang}" ]
          , None = [] : List Text
          } output

in
{ Config = Config
, BindistSpec = BindistSpec
, installFromBindistTo = installFromBindistTo
, setEnv = setEnv
, install = llvmInstall
, configureArgs = llvmConfigureArgs
}
